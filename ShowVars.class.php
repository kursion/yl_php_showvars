<?php
/** ShowVars.class.php
 * author:  Yves Lange
 * version: 1.0
 *
 * NOTE
 * jquery is required to use the ShowVars lib.
 *
 * INSTALLATION
 * Put the "ShowVars.class.php" in the classes directory of the application
 * or website. Add those lines at the end of the PHP file:
 *  <?php
 *  include_once("classes/ShowVars.class.php");
 *  $_SHOWVARS = new ShowVars(get_defined_vars());
 *  $_SHOWVARS->printVar();
 *  ?>
 * 
 *  
 */
class ShowVars {
  // Private variables
  private $parentVars       = array();
  private $parentFunctions  = array();
  private $parentClasses    = array();
  
  // Constructor
  public function __construct($parentVars){
    $this->updateScope($parentVars);
  }

  // Updating the scope with the parent variables
  public function updateScope($parentVars){
    // User variable
    $this->parentVars       = $parentVars;
    
    // User functions
    $aFunctions = get_defined_functions();
    $this->parentFunctions = $aFunctions["user"]; 
    
    // User classes
    $bValue = false;
    foreach(get_declared_classes() as $key=>$value){
      if($bValue == true){
        $this->parentClasses[$value]["methods"] = get_class_methods($value);
      }
      if($value == "Debug"){ $bValue = true; }
    }
  }
  
  /* Function that will print everything from the Debug
   * class (variables, functions, classes)
   */
  public function printVar(){
    ?>
    <style>
      .debug_wrapper{
        border-radius: 5px; padding: 5px; border: #efefef 2px solid; opacity: 0.5; right: 2px;
        width: 120px; background-color: #1f1f1f; color: #efefef; overflow: auto; position: absolute;
        top: 2px; height: 12px; float: left; z-index: 99999; font-size: 12px; overflow-y: hidden;
        overflow-x: hidden;
      }
      .debug_wrapper div{ width: 100%; white-space: pre; }
      .debug_title{ text-align: center; color: #559900; }
      .debug_toggle{ text-align: center; cursor: pointer; }
      .debug_toggle:hover{ font-weight: bold; }
    </style>
    <div class='debug_wrapper'>
      
      <div class='debug_toggle'>SHOWVARS [X]</div>
      <div class='debug_title'>--- VARIABLES ---</div>
      <div ><?php print_r($this->parentVars); ?></div>
      <div class='debug_title'>--- FUNCTIONS ---</div>
      <div><?php print_r($this->parentFunctions); ?></div>
      <div class='debug_title'>--- CLASSES ---</div>
      <div><?php print_r($this->parentClasses); ?></div>
      
    </div>
    <script>
    $(".debug_toggle").toggle(function(){
      $(".debug_wrapper").css("height", "500px");
      $(".debug_wrapper").css("width", "500px");
      $(".debug_wrapper").css("overflow-y", "auto");
      $(".debug_wrapper").fadeTo("fast", 0.9);
    }, function(){
      $(".debug_wrapper").css("width", "120px");
      $(".debug_wrapper").css("height", "12px");
      $(".debug_wrapper").css("overflow-y", "hidden");
      $(".debug_wrapper").fadeTo("fast", 0.5);
    });
    </script>
    <?php
  }
}

?>
